# На этой неделе в KDE: better MTP support

> 2–9 января, основное — прим. переводчика

Many of us are still getting over our new years’ food comas, but we managed to get some cool things done anyway!

## Новые возможности

Task Manager tooltips for windows that are playing audio [now show a volume slider under the playback controls](https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/788) (Noah Davis, Plasma 5.24):

![0](https://pointieststick.files.wordpress.com/2022/01/with-volume-control.png)

## Wayland

In the Plasma Wayland session, [fixed a case where window thumbnails could fail to appear on Task Manager tooltips with certain configurations](https://bugs.kde.org/show_bug.cgi?id=446061) (David Edmundson, Plasma 5.24)

In the Plasma Wayland session, the System Tray item for showing and hiding the virtual keyboard [now becomes active only in tablet mode](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1335) (Nate Graham, Plasma 5.24)

## Исправления ошибок и улучшения производительности

Okular is now more reliable about [opening](https://invent.kde.org/graphics/okular/-/merge_requests/532) [and](https://invent.kde.org/graphics/okular/-/merge_requests/535) [signing](https://invent.kde.org/graphics/okular/-/merge_requests/528) different kinds of password-protected documents (Albert Astals Cid, Okular 21.12.1)

Connectivity with MTP devices now works much better overall: they [now display correctly in the Disks & Devices applet](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1330), opening one in Dolphin [now refreshes the view automatically when you follow the provided instructions by unlocking your device and allowing access](https://bugs.kde.org/show_bug.cgi?id=440794), and [the instructions are now clearer and more actionable](https://invent.kde.org/network/kio-extras/-/merge_requests/140) (Harald Sitter and James John, Plasma 5.24 and Dolphin 22.04)

Bluetooth devices that connect in a nonstandard way like PlayStation Dualshock 3 Wireless Controllers [now appear in the Bluetooth applet after being connected](https://bugs.kde.org/show_bug.cgi?id=432715) (Bart Ribbers, Plasma 5.23.5)

Turning a monitor off and back on [no longer sometimes causes certain windows to be resized](https://bugs.kde.org/show_bug.cgi?id=447419) (Xaver Hugl, Plasma 5.24)

Clicking the Pause button on System Settings’ File Search page [now actually pauses indexing](https://bugs.kde.org/show_bug.cgi?id=443693) (Yerrey Dev, Plasma 5.24)

You [can now change the user or group of a file or folder on the desktop](https://bugs.kde.org/show_bug.cgi?id=444624) (Ahmad Samir, Frameworks 5.91)

Snap apps [no longer inappropriately appear as mounted volumes in Places panels](https://invent.kde.org/frameworks/solid/-/merge_requests/65) (Kai Uwe Broulik, Frameworks 5.91)

Re-mapping keys with the System Settings Advanced Keyboard page [now causes any swapped modifier keys to be correctly handled by global keyboard shortcuts](https://bugs.kde.org/show_bug.cgi?id=426684) (Fabian Vogt, Frameworks 5.90)

## Улучшения пользовательского интерфейса

The Battery and Brightness applet [now turns into just a Brightness applet on computers with no batteries but any brightness controls](https://bugs.kde.org/show_bug.cgi?id=415073) (Aleix Pol Gonzalez, Plasma 5.24):

![1](https://pointieststick.files.wordpress.com/2022/01/brightness-1.png)

Plasma applets with scrollable views [now](https://invent.kde.org/plasma/plasma-nm/-/merge_requests/96) [use a](https://invent.kde.org/plasma/bluedevil/-/merge_requests/66) [more consistent](https://invent.kde.org/plasma/plasma-pa/-/merge_requests/107) [style](https://invent.kde.org/plasma/plasma-workspace/-/merge_requests/1328) (Carl Schwan, Plasma 5.24):

![2](https://pointieststick.files.wordpress.com/2022/01/scrollbar.png)

The Scale effect is [now used by default for window opening and closing](https://invent.kde.org/plasma/kwin/-/merge_requests/1845), instead of the old Fade effect (Влад Загородний, Plasma 5.24)

Items [are now selected after being moved or created on the desktop](https://bugs.kde.org/show_bug.cgi?id=434513) (Derek Christ, Plasma 5.24)

When you enable auto-login, [you are now warned about some changes you might want to make to your KWallet setup](https://invent.kde.org/plasma/sddm-kcm/-/merge_requests/20) (Nate Graham, Plasma 5.24):
<!-- Придётся словами описать, что за изменения -->

![3](https://pointieststick.files.wordpress.com/2022/01/message.png)

Yakuake’s System Tray icon [is now monochrome](https://bugs.kde.org/show_bug.cgi?id=427485) (Артём Гринёв and Bogdan Covaciu, Frameworks 5.91:

![5](https://pointieststick.files.wordpress.com/2022/01/yakuake-tray-icon.png)

Menus in Qt Quick apps [now have the same size and appearance as menus in Qt Widgets apps](https://bugs.kde.org/show_bug.cgi?id=447289) (Nate Graham, Frameworks 5.91)

Sliders in Qt Quick apps [can now be manipulated by scrolling over them](https://bugs.kde.org/show_bug.cgi?id=417211), just like sliders elsewhere (Nate Graham, Frameworks 5.91)

## Как вы можете помочь

Прочитайте [эту статью](https://kde.ru/join), чтобы узнать, как включиться в разработку важного для вас проекта. Для KDE крайне важен каждый участник; вы не число и не винтик в машине! Совершенно необязательно быть программистом. Я не был им, когда начинал. Попробуйте, вам понравится! Мы не кусаемся!

Наконец, если вы находите программное обеспечение KDE полезным, рассмотрите возможность [оказать финансовую поддержку](https://kde.org/ru/community/donations/) [фонду KDE e.V](https://ev.kde.org/).

_Автор:_ [Nate Graham](https://pointieststick.com/author/pointieststick/)  
_Перевод:_ [Максим Маршев](https://t.me/msmarshev)  
_Источник:_ https://pointieststick.com/2022/01/07/this-week-in-kde-better-mtp-support/  

<!-- 💡: System Settings → Параметры системы -->
<!-- 💡: applet → виджет -->
<!-- 💡: icon → значок -->
<!-- 💡: screenshot → снимок экрана -->
<!-- 💡: system tray → системный лоток -->
<!-- 💡: task manager → панель задач -->
<!-- 💡: thumbnail → миниатюра -->
